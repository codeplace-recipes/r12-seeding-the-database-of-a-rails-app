require 'open-uri'
require 'json'

users = {
  user1: { email: 'user1@example.com', password: '123456' },
  user2: { email: 'user2@example.com', password: '123456' },
  user3: { email: 'user3@example.com', password: '123456' }
}

users.each do |_k, v|
  User.create!(
    email: v[:email],
    password: v[:password],
    password_confirmation: v[:password]
  )
end

user1 = User.find_by_email(users[:user1][:email])
user2 = User.find_by_email(users[:user2][:email])
user3 = User.find_by_email(users[:user3][:email])

Article.create(
  [
    {
      name: 'Imagine what you could build if you learned Ruby on Rails...',
      content: 'Learning to build a modern web application is daunting. Ruby on Rails makes it much easier and more fun. It includes everything you need to build fantastic applications, and you can learn it with the support of our large, friendly community.',
      published_on: Time.current,
      user: user1
    },
    {
      name: 'Learn by building - HTML5, CSS3, JavaScript / jQuery',
      content: 'Becoming a junior web developer is impossible without learning about basic blocks of every website out there. Sure all the back-end technologies are cool, but front-end basics are a must!',
      published_on: Time.current,
      user: user2
    },
    {
      name: 'Ember JS - A framework for creating ambitious web applications.',
      content: 'Ember.js is an open-source JavaScript web framework, based on the Model–view–viewmodel (MVVM) pattern. It allows developers to create scalable single-page web applications[1] by incorporating common idioms and best practices into the framework.',
      published_on: Time.current,
      user: user3
    },
    {
      name: 'Angular 2 - One framework. Mobile & desktop.',
      content: 'Learn one way to build applications with Angular and reuse your code and abilities to build apps for any deployment target. For web, mobile web, native mobile and native desktop.',
      published_on: Time.current,
      user: user1
    },
    {
      name: 'React - A JavaScript library for building user interfaces',
      content: 'React makes it painless to create interactive UIs. Design simple views for each state in your application, and React will efficiently update and render just the right components when your data changes.',
      published_on: Time.current,
      user: user2
    },
    {
      name: 'Ionic - The top open source framework for building amazing mobile apps.',
      content: 'Know how to build websites? Then you already know how to build mobile apps. Ionic Framework offers the best web and native app components for building highly interactive native and progressive web apps with Angular.',
      published_on: Time.current,
      user: user3
    }
  ]
)

@countries = JSON.parse(open('http://data.okfn.org/data/core/country-list/r/data.json').read)
@countries.each do |country|
  Country.create(name: country['Name'], code: country['Code'])
end

30.times do
  Article.create!(name: Faker::Lorem.sentence, content: Faker::ChuckNorris.fact,
                  published_on: Faker::Time.between(2.days.ago, Date.today, :day),
                  user: User.offset(rand(User.count)).first)
  puts 'Article created!'
end
