class Article < ApplicationRecord
  belongs_to :user
  scope :published, -> { where('published_on <= ?', Time.now) }
end
